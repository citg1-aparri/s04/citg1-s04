package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6177744020363909937L;

	public void init() {
		System.out.println("------------------------------------");
		System.out.println("DetailsServlet initialized");
		System.out.println("------------------------------------");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		String fName = System.getProperty("firstname");
		HttpSession session = req.getSession();
		String lName = session.getAttribute("lastname").toString();
		ServletContext srvContext = getServletContext(); 
		String email = srvContext.getAttribute("email").toString();
		String contact = req.getParameter("contact");
		
		String brand = srvContext.getInitParameter("branding");
		PrintWriter out = res.getWriter();
		out.println("<h1>" + brand + "</h1>" +
		"<p>First Name: " + fName + " </p>"+
		"<p>Last Name: " + lName + "</p>"+
		"<p>Contact: " + contact + "</p>" +
		"<p>Email: " + email + "</p>"
		);
	}
	
	public void destroy() {
		System.out.println("------------------------------------");
		System.out.println("DetailsServlet destroyed");
		System.out.println("------------------------------------");
	}
}
