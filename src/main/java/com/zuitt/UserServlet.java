package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	
	public void init() {
		System.out.println("------------------------------------");
		System.out.println("UserServlet initialized");
		System.out.println("------------------------------------");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		String fName = req.getParameter("firstname");
		String lName = req.getParameter("lastname");
		String email = req.getParameter("email");
		String contact = req.getParameter("contact");
		
		System.getProperties().put("firstname", fName);
		
		HttpSession session = req.getSession();
		session.setAttribute("lastname", lName);
		
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", email);
		
		res.sendRedirect("details?contact=" + contact);
	}
	
	public void destroy() {
		System.out.println("------------------------------------");
		System.out.println("UserServlet destroyed");
		System.out.println("------------------------------------");
	}
}
